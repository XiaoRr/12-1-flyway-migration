create table contracts(
    id int primary key,
    name varchar(128) not null,
    client_id int not null,
    FOREIGN KEY (client_id) REFERENCES clients (id)
)