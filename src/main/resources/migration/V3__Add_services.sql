create table services(
     id int primary key ,
     description varchar(128)
);

alter table contracts
add service_id int,
add foreign key (service_id) references services(id);
