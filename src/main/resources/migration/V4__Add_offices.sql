create table offices(
    id int primary key,
    country varchar(64),
    city varchar(64)
);

create table staffs(
    id int primary key ,
    first_name varchar(64),
    last_name varchar(64),
    office_id int,
    foreign key(office_id) references offices(id)
);