create table contract_service_offices(
    id int primary key ,
    contract_id int,
    office_id int,
    foreign key (contract_id) references contracts(id),
    foreign key (office_id) references offices(id)
)